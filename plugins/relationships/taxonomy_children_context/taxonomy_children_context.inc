<?php

/**
 * @file
 * Plugin to provide a relationship handler to return all children of a given
 * term.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Taxonomy Children Context'),
  'description' => t('Returns a tree with all children for a given term.'),
  'required context' => array(
    new ctools_context_required(t('Term'), 'entity:taxonomy_term'),
  ),
  'context' => 'taxonomy_children_context',
  'edit form' => 'taxonomy_children_context_settings_form',
  'defaults' => array('use_parent' => 1, 'operator' => 'and'),
  'keyword' => 'term_children',
);

/**
 * Return the terms context based on the given term context.
 */
function taxonomy_children_context($contexts, $conf) {

  if (empty($contexts[0]->data)) {
    return ctools_context_create_empty('terms');
  }

  $parent = $contexts[0]->data;
  $children = taxonomy_get_tree($parent->vid, $parent->tid);

  $data = new stdClass();

  $data->operator = $conf['operator'];

  $data->value = array();

  if ($conf['use_parent'] === '1') {
    $data->value[] = $parent->tid;
  }

  foreach ($children as $child) {
    $data->value[] = $child->tid;
  }

  return ctools_context_create('terms', $data);
}


/**
 * Settings form for the relationship.
 */
function taxonomy_children_context_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['use_parent'] = array(
    '#type' => 'radios',
    '#title' => t('Use parent term'),
    '#default_value' => $conf['use_parent'],
    '#options' => array(
      0 => t('Do not use parent'),
      1 => t('Use parent'),
    ),
    '#description' => t('Shall the parent added to the term tree?'),
  );

  $form['operator'] = array(
    '#type' => 'radios',
    '#title' => t('Operator'),
    '#default_value' => $conf['operator'],
    '#options' => array(
      'and' => t('And'),
      'or' => t('Or'),
    ),
    '#description' => t('Which operator shall be used to chain the tree?.'),
  );

  return $form;
}
